# Factorio-Discord-Bots

This is just a discord bot I created to compliment the game Factorio.
There exists blueprints, and using this bot, those blueprint strings can be saved outside of the game for easy sharing with others.
Other functions include storing and printing symbols for easy copying.
