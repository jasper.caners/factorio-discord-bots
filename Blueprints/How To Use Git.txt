git branch <branch name> --> Creates new branch called <branch name>

git checkout <branch name> --> Switch branches to <branch name>

git pull git@gitlab.com:my.username/repositoryname.git <branch name> --> Pulls any changes
	from GitLab into the branch <branch name> on the local machine

git commit -a -m "Message" --> Commits all changes on current branch with the message
	"Message" so that the changes are ready to be pushed onto GitLab

git push git@gitlab.com:my.username/repositoryname.git <branch name> --> Pushes all commits
	on local machine in currently accessed branch to the branch <branch name> on GitLab
	in the repository that is selected.