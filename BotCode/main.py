import enum
import discord
from discord.ext import commands
import os
import sys

from dotenv import load_dotenv
load_dotenv()


#A class to deal with the different types of help commands.
#May move this to a cog in the future if that works.
class CustomHelpCommand(commands.HelpCommand):

    def __init__(self):
        super().__init__()

    async def send_bot_help(self, mapping):
        global commandDict
        currentMessage = ""
        for cog in mapping:
            if cog != None:
                currentMessage = currentMessage + await self.buildCogHelpMessage(cog)
        if currentMessage != "":
            await self.get_destination().send(currentMessage)


    async def send_cog_help(self, cog):
            if cog != None:
                currentMessage = await self.buildCogHelpMessage(cog)
                #await self.get_destination().send(f'\t- {commandName}: {commandInfo}')
                await self.get_destination().send(currentMessage)

    async def send_group_help(self, group):
        await self.get_destination().send(f'{group.name}: {[command.name for index, command in enumerate(group.commands)]}')

    async def send_command_help(self, command):
        await self.get_destination().send(command.name)

    async def getItem(self, commandName):
        global commandDict
        commandInfo = commandDict.get(commandName)
        if commandInfo == None:
            commandInfo = "No data on instruction"
        return commandInfo

    async def buildCogHelpMessage(self, cog):
        if cog != None:
            currentMessage = f'**{cog.qualified_name}:**' + "\n"
            #await self.get_destination().send(f'**{cog.qualified_name}:**')
            for command in cog.get_commands():
                commandName = command.name
                commandInfo = await self.getItem(commandName)
                currentMessage = currentMessage + f'\t- {commandName}: {commandInfo}' + "\n"
            return currentMessage


#Can change the command prefix here
client = commands.Bot(command_prefix = '/', help_command = CustomHelpCommand())

#This command dictionary may be used in the future for a custom help command
commandDict = {'help':"Displays all possible commands that have been currently implemented.",
    'close': "Stops the bot program.",
    'ListBPrint': "Lists all of the blueprints available.",
    'GetBPrint': "Accepts an argument x. Returns a message containing the contents of the blueprint file named x.",
    'MakeBPrint': "Saves a blueprint to memory. Accepts two arguments, x y. x is the blueprint name, and y is the blueprint string.",
    'DeleteBPrint': "Deletes a specified blueprint from memory, after a confirmation code is passed. Accepts an argument, x. x is the file which to delete.",
    'ListSymbols': "Lists the possible symbols that the bot has stored.",
    'GetSymbol': "Prints the specified symbol to the chat. Accepts one argument, x, which specifies which symbol to send to the chat."}

OWNER = 634979532620693504 #My owner id

#Again, used to ensure that the owner is running the commnad.
def owner(self, ctx):
        return ctx.author.id == OWNER


#Load a cog
@client.command()
async def load(ctx, extension):
    if await isOwner(ctx):
        client.load_extension(f'cogs.{extension}')

#Unload a cog
@client.command()
async def unload(ctx, extension):
    if await isOwner(ctx):
        client.unload_extension(f'cogs.{extension}')

#Reload a cog
@client.command()
async def reload(ctx, extension):
    if await isOwner(ctx):
        client.unload_extension(f'cogs.{extension}')
        client.load_extension(f'cogs.{extension}')


#When run, this loads all cogs in the cogs directory
for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')


#Returns true if message sender is OWNER above:
async def isOwner(ctx):
    return ctx.message.author.id == OWNER


#Needed to run the bot. MUST BE LAST LINE!
client.run(os.getenv('TOKEN'))