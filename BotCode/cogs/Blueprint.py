import discord
from discord.ext import commands
import os


currentConfirmationRequests = []

class Blueprint(commands.Cog):

    def __init__(self, client):
        self.client = client

    #Global variable to notify user that the blueprint file that they were trying to access did not exist
    getBPrintFileName = ""
    #Global variable to set cap of how many blueprint files are allowed
    maxBlueprintNum = 20

    #Global variable to keep track of the current deletion requests and such stuff.
    #Items in the list should be in the form: [self,command]
    


    @commands.command()
    #ListBPrint command
    async def ListBPrint(self, ctx):
        #Look in the Blueprints folder for all of the blueprints
        blueprintList = os.listdir('../Blueprints')
        #Get the number of blueprints to tell the user
        numBlueprint = len(blueprintList)
        stringBlueprint = '**The number of blueprints is: ' + str(numBlueprint) + '**\n**Blueprint list:**'
        #For every single blueprint in that folder, append its name to the end of the bot message
        for bp in blueprintList:
            stringBlueprint += ('\n')
            dotFilePos = bp.rfind('.')
            stringBlueprint += bp[0:dotFilePos]
        #Send the completed message to the server
        await ctx.send(stringBlueprint)


    @commands.command()
    async def MakeBPrint(self, ctx, fileName, *, blueprintString):
        #First, count the number of blueprints currently in the system
        blueprintList = os.listdir('../Blueprints')
        if Blueprint.maxBlueprintNum > len(blueprintList):
            #Clean up the file name, attach the .txt if it is missing it
            fileName = fileName.strip()
            if not fileName.endswith('.txt'):
                fileName += '.txt'
            #Save the file name in a global variable
            #That way, if it fails to find the blueprint file, the bot can inform the user
            # the name of the file that it could not find.
            #Make the file path name with the folder location in front
            filePathName = "../Blueprints/" + fileName

            fileExists = os.path.isfile(filePathName)
            if fileExists:
                await ctx.send("Blueprint: " + fileName + " already exists. Please use a different blueprint name.")
            else:
                try:
                    #If the file exists, open the file in write mode
                    blueprintFile = open(filePathName,"w")
                    # Write to the file using the blueprintString suggested
                    blueprintFile.write(blueprintString)
                    #Close the file
                    blueprintFile.close()
                except FileNotFoundError:
                    await ctx.send("File: " + fileName + " does not exist.")

        else:
            tooManyBPrintBy = Blueprint.maxBlueprintNum - len(blueprintList) + 1
            await ctx.send("Too many blueprints exist currently. Please remove {num:d}.".format(num = tooManyBPrintBy))

    @commands.command()
    #DeleteBPrint command. Format is: /DeleteBPrint ABlueprint
    #The space between the command and the blueprint name is needed
    async def DeleteBPrint(self, ctx, *,fileName):
        global currentConfirmationRequests
        #Clean up the file name, attach the .txt if it is missing it
        fileName = fileName.strip()
        if not fileName.endswith('.txt'):
            fileName += '.txt'
        #Save the file name in a global variable
        #That way, if it fails to find the blueprint file, the bot can inform the user
        # the name of the file that it could not find.
        Blueprint.getBPrintFileName = fileName
        #Make the file path name with the folder location in front
        filePathName = "../Blueprints/" + fileName

        fileExists = os.path.isfile(filePathName)
        #If the file exists, inform the user that they must confirm the deletion of the file, with either a "Y" or a "N".
        if fileExists:
            try:
                await ctx.send("Please confirm if you want to delete " + fileName + " from the blueprints list.")
                await ctx.send("Respond with a Y to confirm the deletion, or a N to cancel.")

                currentConfirmationRequests.append([ctx,"DeleteBPrint " + fileName])

            except FileNotFoundError:
                await ctx.send("File: " + fileName + " does not exist.")
        else:
            await ctx.send("File: " + fileName + " does not exist.")



    @commands.Cog.listener()
    async def on_message(self, message):
        global currentConfirmationRequests
        if message.author != self.client.user:
            #If the message does not originate from the bot
            if str(message.content) =="Y" or str(message.content) =="N":
                #If the message is purely either a Y or a N
                for pending in currentConfirmationRequests:
                    #Go over every pending confirmation request, checking if the author and channel is the same.
                    isSameChannel = message.channel == pending[0].channel
                    isSameAuthor = message.author == pending[0].author
                    if isSameChannel and isSameAuthor:
                        #If the pending request has the command start with DeleteBPrint (Note that more commands can use this confirmation system)
                        if pending[1].startswith("DeleteBPrint "):
                            #Get the context and the file name from the pending request
                            ctx = pending[0]
                            fileName = (pending[1].split())[1]
                            #If the initial command from above was a Y, meaning to delete, start deleting
                            if str(message.content) == "Y":
                                
                                filePathName = "../Blueprints/" + fileName
                                fileExists = os.path.isfile(filePathName)
                                #If the file exists (which it should), continue deleting
                                if fileExists:
                                    try:
                                        await ctx.send("Deleting " + fileName + " from the blueprints list.")
                                        #Remove the file from the folder
                                        os.remove(filePathName)

                                    except FileNotFoundError:
                                        await ctx.send("File: " + fileName + " does not exist.")
                                else:
                                    await ctx.send("File: " + fileName + " does not exist.")
                            #Else if the message is a N from above (Yes, this is the only other option, but this makes it more clear)
                            elif str(message.content) == "N":
                                #Send a simple message saying that the file deletion request was cancelled
                                await ctx.send("Cancelled deletion of " + fileName)
                        #Remove the request from the list.
                        currentConfirmationRequests.remove(pending)
                        break


    @commands.command()
    #GetBPrint command. Format is: /GetBPrint ABlueprint
    #The space between the command and the blueprint name is needed
    async def GetBPrint(self, ctx, *,fileName):
        #Clean up the file name, attach the .txt if it is missing it
        fileName = fileName.strip()
        if not fileName.endswith('.txt'):
            fileName += '.txt'
        #Save the file name in a global variable
        #That way, if it fails to find the blueprint file, the bot can inform the user
        # the name of the file that it could not find.
        Blueprint.getBPrintFileName = fileName
        #Make the file path name with the folder location in front
        filePathName = "../Blueprints/" + fileName

        fileExists = os.path.isfile(filePathName)

        if fileExists:
            try:
                #This try catch block does not seem to function as expected, 
                # but I am keeping it in here just in case it does start to work.
                blueprintFile = open(filePathName,"r")
                #Read the blueprint file, and send it out to the server, then close the file
                await ctx.send(blueprintFile.read())
                blueprintFile.close()
            except FileNotFoundError:
                await ctx.send("File: " + fileName + " does not exist.")
        else:
            await ctx.send("File: " + fileName + " does not exist.")

    @GetBPrint.error
    #Error if the GetBPrint command is unable to find the necessary file, or the file name was missing
    async def on_getbprint_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("Please specify a blueprint name.")
        elif isinstance(error, commands.CommandInvokeError):
            await ctx.send("File: " + Blueprint.getBPrintFileName + " does not exist.")

def setup(client):
    client.add_cog(Blueprint(client))