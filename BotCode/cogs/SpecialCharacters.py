import discord
from discord.ext import commands
import os


class SpecialCharacters(commands.Cog):

    symDict = {}

    def __init__(self, client):
        self.client = client
        #Setting up a global variable to use in the future.
        #If more characters are needed, just add to the dictionary the name, and then the symbol.
        global symDict
        symDict = {"leftArrow":"←","rightArrow":"→","upArrow":"↑","downArrow":"↓","horizontalArrow":"↔","verticalArrow":"↕",
        "upLeftArrow":"↖","upRightArrow":"↗","downLeftArrow":"↘","downRightArrow":"↙",
        "leftArrowBar":"↤","rightArrowBar":"↦","upArrowBar":"↥","downArrowBar":"↧"}

    @commands.command()
    async def ListSymbols(self, ctx):
        count = 0
        message = ""
        #For ever key in the global dictionary, add it to the string to be printed off
        for key in symDict.keys():
            count += 1
            message += key +": " + symDict[key] +"\t"
            # If the number of commands currently added is 5, add a new line character and reset the counter
            if count == 5:
                count = 0
                message += "\n"
        # Send the final created message
        await ctx.send(message)



    @commands.command()
    async def GetSymbol(self, ctx, symName):
        #Cleaning up the string, just in case
        symName = symName.strip()
        foundKey = False
        #For every key in the dictionary, if it matches what the use is looking for, send that character.
        for key in symDict.keys():
            if(key == symName):
                await ctx.send(symDict[key])
                #Set foundKey to True, as the key has been found
                foundKey = True
        #If the key was not found, print a simple error message.
        if not foundKey:
            await ctx.send("Could not find a symbol named: " + symName +"\nPlease try a different symbol name.")
    


def setup(client):
    client.add_cog(SpecialCharacters(client))