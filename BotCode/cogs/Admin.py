import discord
from discord.ext import commands

OWNER = 634979532620693504

class Admin(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    #Checks
    
    def owner(self, ctx):
        return ctx.author.id == OWNER


    #Events
    @commands.Cog.listener()
    async def on_ready(self):
        print('We have logged in as {0.user}'.format(self.client))

    #Commands
    @commands.command()
    async def close(self, ctx):
        exit(0)


def setup(client):
    client.add_cog(Admin(client))